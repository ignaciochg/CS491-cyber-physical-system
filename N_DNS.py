#!/usr/bin/env python2


FOR_PRODUCTION=True

class N_DNS:
   records = {
      # "hostname": ("hostname", "ip", int(port))
      # "":("", "", ),
   } 
   def __init__(self, prduction=FOR_PRODUCTION):
      if prduction:
         self.records = {
            "CC":("CC", "10.0.0.1", -1),
            "DA":("DA", "10.0.0.20", 20000),
            "R1":("R1", "10.0.0.11", 20001),
            "R2":("R2", "10.0.0.12", 20002),
            "R3":("R3", "10.0.0.13", 20003),
            "R4":("R4", "10.0.0.14", 20004)
         }
      else:
         self.records = {
            "CC":("CC", "127.0.0.01", -1),
            "DA":("DA", "127.0.0.01", 20000),
            "R1":("R1", "127.0.0.01", 20001),
            "R2":("R2", "127.0.0.01", 20002),
            "R3":("R3", "127.0.0.01", 20003),
            "R4":("R4", "127.0.0.01", 20004)
         } 
      #print "init Nacho Local DNS Object"

   def whoIs(self, name): # get ip of name
      if name in self.records:
         return self.records[name]
      else:
         return False
   def insertRecord(self, name, IP, port):
      if name != "" and IP != "" and isinstance(port, int):
         if name in self.records:
            return False
         else:
            self.records[name] = (name ,IP, port)
         return True
      else:
         return False
   def printRecords(self):
      print(self.records)


if __name__ == "__main__":
   dns = N_DNS()
   dns.printRecords()
   print("will look for 'R1':")
   record = dns.whoIs("R1")
   print(record)
   print("Will create record testName: ('testName','123.123.123.123', 1234)")
   dns.insertRecord("testName", "123.123.123.123", 1234)
   print("retrieving testName:")
   record2 = dns.whoIs("testName")
   print(record2)
   print("Done.")
