#!/usr/bin/env python2

import struct # for packing and unpacking the protocol 
#import binascii
import time



REQUEST_BYTE = struct.pack("B",  1) # 00000001
RESPONSE_BYTE = struct.pack("B", 3) # 00000011
ERROR_BYTE = struct.pack("B", 7)    # 00000111

PRETTY_PRINT=False

def buildRequest(indexList): 
   # will take body(list of indexes) as input and then add header (req+length)
   # make packed of each index
   # get length and request type
   # combine bytes into one message 
   # return 
   requestBody = str('')
   length = struct.pack("!B", len(indexList)+2) # request header =2 , + number of requests (1 each), in bytes

   for index in indexList:
      requestBody = requestBody + struct.pack("!B", index)

   request = REQUEST_BYTE + length  + requestBody
   return request


def buildResponse(listOfIndexValue): #list of tuples (index, value)
   respBody = str('')
   length = struct.pack("!B", (len(listOfIndexValue)*5) +2) # request header =2 , + number of responses (5 each), in bytes


   for tup in listOfIndexValue:
      index = struct.pack("!B", tup[0])
      value = struct.pack("!f", tup[1])
      #print("adding", tup[0], tup[1])
      bod = index + value
      respBody = respBody + bod
   response = RESPONSE_BYTE + length + respBody
   return response

def decodeBody(binary): # will decode body and return based on mType
   # if request: return list of indeces
   # if response: return list of tuples, each tuple is (index, value)
   mType = binary[0]
   binLength = binary[1:2]
   body = binary[2:]
   content = []
   error = False

   if(len(binary) <2):
      # print("error: can not decode less than 2 bytes")
      error = True

   elif(mType == REQUEST_BYTE):
      # print("will try to decode as request")
      try:
         length = int(struct.unpack("!B", binLength)[0])
         if(length-2 != len(body)):
            # print("real length and length field dont match")
            error = True
         if not error:       
            bodylength = length-2
            for x in range(0, bodylength):
               indexVal = int(struct.unpack("!B", body[x])[0])
               content.append(indexVal)
      except:
         # print("could not make sense of request")
         error = True

   elif(mType == RESPONSE_BYTE):
      # print("will try to decode as response")
      try:
         length = int(struct.unpack("!B", binLength)[0])
         if ((len(body) % 5) != 0):
            # print("body its not devisible by 5")
            error = True

         # print(length-2, len(body))
         # print(toPrettyHex(body))
         if(length-2 != len(body)):
            # print("real length and length field dont match")
            error = True
         if not error:
            bodylength = length-2
            times = bodylength/5
            for x in range(0, times):
               botIndex = (x*5)
               topPlusOneIndex = ((x+1)*5)
               oneRecord = struct.unpack("!Bf", body[botIndex:topPlusOneIndex])
               content.append((int(oneRecord[0]),float(oneRecord[1])))
      except:
         # print("could not make sense of response")
         error = True
   elif(mType == ERROR_BYTE):
      return ERROR_BYTE
   else:
      # print("error: can not detect type of binary")
      error = True

   if(error):
      # print("error detected will return ERROR_BYTE")
      return ERROR_BYTE
   else:
      return content

def messageType(binary):# compare return to REQUEST_BYTE, RESPONSE_BYTE, ERROR_BYTE
   return binary[0] 

def decodeMessage(binary): # will return messageType, content
# messageType is decoded from binary
# content is ERROR_BYTE if error in decoding, a list of indexes if request,
#and list of tuple(index,value) if response
   # will return tupple
   # will return type, body
   # body will be decoded by another function
   bodyContent = decodeBody(binary)
   return binary[0], bodyContent

def validContent(mType, content):
   if(content == ERROR_BYTE):
      return False
   if(mType == ERROR_BYTE):
      return False
   return True

def toPrintableHex(binary):
   return binary.encode("hex")
def toPrettyHex(binary):
   return "0x"+binary.encode("hex")


dash = '-' * 40
def printOneResponse(tup):
   pass
# def printOneRequest(time, index): # all of these should have the same time
#    pass

def printRespHeader():
   print(dash)
   print("{:<17s}{:>8s}{:>12s}".format("Epoch Time", "Index","Value"))
   print(dash)
   
# def printReqHeader():
#    pass
def printResponseTable(respList, pretty=PRETTY_PRINT):
   if(pretty):
      printRespHeader()
   for tup in respList:
      tim = time.time()
      if(pretty):
         print("{:<17f}{:^8d}{:>12f}".format(tim, tup[0],tup[1]))
      else:
         print str(tim)+":"+str(tup[0])+":"+str(tup[1])


# def printRequestTable(reqList):
#    pass





if __name__ == "__main__":
   # print("Request Byte: ")
   # print("0x"+ toPrintableHex(REQUEST_BYTE))
   # print("length: "+str(len(REQUEST_BYTE))+"Byte")
   # print("")

   # print("Response Byte:")
   # print("0x"+ toPrintableHex(RESPONSE_BYTE))
   # print("length: "+str(len(RESPONSE_BYTE))+"Byte")
   # print("")

   # print("Error Byte")
   # print("0x"+ toPrintableHex(ERROR_BYTE))
   # print("length: "+str(len(ERROR_BYTE))+"Byte")
   # print("")

   # ft = -50.69
   # ft2 = 69.50
   # ft3 = 4
   # print("Packing "+str(ft)+", "+str(ft2))
   # pcked = []
   # pcked = struct.pack("ff", ft, ft2)
   # print("packed:")
   # print("0x"+ toPrintableHex(pcked))
   # print("lenght of packed (8): "+str(len(pcked)))
   # print("UNpacked:")
   # unpcked = struct.unpack("ff", pcked)
   # pcked2 = struct.pack_into("f", pcked, 4, ft3)
   # print("0x"+ toPrintableHex(pcked2))

   # print(unpcked)

# testing request maker and decoder for request
   indexList = [1,2,3,4,5,6]
   print("will request:", indexList)
   print("makin binary request...")
   binRequest = buildRequest(indexList)
   print("request in hex",toPrettyHex(binRequest))
   print("will decode it now....")
   mType, content = decodeMessage(binRequest)

   print(toPrettyHex(mType),content)
   if(mType == REQUEST_BYTE and content != ERROR_BYTE):
      print("It's of type REQUEST")
      print("content:", content)
   elif(mType == RESPONSE_BYTE):
      print("It's of type RESPONSE" and content != ERROR_BYTE)
      print("content:", content)
   else:
      print("error decoding binary")

   print('')
   print('')

# testing response maker and decoder for request
   responses = [
                  (1, 0.55),
                  (2, 1.64),
                  (3, 2.73),
                  (4, 3.82),
                  (5, 4.91)
               ]

   print("will Responde with:", responses)
   print("makin binary response...")
   binResponse = buildResponse(responses)
   print("request in hex",toPrettyHex(binResponse))
   print("will decode it now....")
   mType, content = decodeMessage(binResponse)

   print(toPrettyHex(mType),content)
   if(mType == REQUEST_BYTE ):
      print("It's of type REQUEST")
      if(validContent(mType, content)):
         print("content:", content)
      else:
         print("can not print content, not valid. AKA error in body")
   elif(mType == RESPONSE_BYTE ):
      print("It's of type RESPONSE")
      if(validContent(mType, content)):
         print("content:", content)
      else:
         print("can not print content, not valid. AKA error in body")
   else:
      print("error decoding binary")



