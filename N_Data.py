#!/usr/bin/env python2

# will hold the data in centralized location
# each node will tell this class their name, 
# it will use hostname to only allow access to the right host
# this measn its a centralized data center but only the responsible party can access the data 
# this means that R1 can only read R1 field, aka it can not read R2 R3 R4 information

INVALID_INDEX = "INVALID-INDEX-NO-VALUE"

class N_Data:
   # data = {
   #    11: (11, 1.00    , "R1" , "Bus1"),
   #    12: (12, 0.00    , "R1" , "Bus1"),
   #    13: (13, 71.95   , "R1" , "Bus1"),
   #    14: (14, 24.07   , "R1" , "Bus1"),
   #    21: (21, 1.00    , "R1" , "Bus2"),
   #    22: (22, 9.67    , "R1" , "Bus2"),
   #    23: (23, 163.00  , "R1" , "Bus2"),
   #    24: (24, 14.46   , "R1" , "Bus2"),
   #    31: (31, 1.00    , "R2" , "Bus3"),
   #    32: (32, 4.77    , "R2" , "Bus3"),
   #    33: (33, 85.00   , "R2" , "Bus3"),
   #    34: (34, -3.65   , "R2" , "Bus3"),
   #    41: (41, 0.99    , "R2" , "Bus4"),
   #    42: (42, -2.41   , "R2" , "Bus4"),
   #    43: (43, 0.00    , "R2" , "Bus4"),
   #    44: (44, 0.00    , "R2" , "Bus4"),
   #    51: (51, 0.98    , "R3" , "Bus5"),
   #    52: (52, -4.02   , "R3" , "Bus5"),
   #    53: (53, -90.00  , "R3" , "Bus5"),
   #    54: (54, -30.00  , "R3" , "Bus5"),
   #    61: (61, 1.01    , "R3" , "Bus6"),
   #    62: (62, 1.93    , "R3" , "Bus6"),
   #    63: (63, 0.00    , "R3" , "Bus6"),
   #    64: (64, 0.00    , "R3" , "Bus6"),
   #    71: (71, 0.99    , "R4" , "Bus7"),
   #    72: (72, 0.62    , "R4" , "Bus7"),
   #    73: (73, -100.00 , "R4" , "Bus7"),
   #    74: (74, -35.00  , "R4" , "Bus7"),
   #    81: (81, 1.00    , "R4" , "Bus8"),
   #    82: (82, 3.80    , "R4" , "Bus8"),
   #    83: (83, 0.00    , "R4" , "Bus8"),
   #    84: (84, 0.00    , "R4" , "Bus8"),
   #    91: (91, 0.96    , "R4" , "Bus9"),
   #    92: (92, -4.35   , "R4" , "Bus9"),
   #    93: (93, 0.00    , "R4" , "Bus9"),
   #    94: (94, 0.00    , "R4" , "Bus9"),
   # }
   data = {
      11: (11, 1.00    , "R1" , "Bus1"),
      12: (12, 0.00    , "R1" , "Bus1"),
      13: (13, 0.67    , "R1" , "Bus1"),
      14: (14, 0.0     , "R1" , "Bus1"),
      21: (21, 1.00    , "R1" , "Bus2"),

      23: (23, 1.63    , "R1" , "Bus2"),
      24: (24, 0.0     , "R1" , "Bus2"),
      31: (31, 1.00    , "R2" , "Bus3"),

      33: (33, 0.85    , "R2" , "Bus3"),
      34: (34, 0.0     , "R2" , "Bus3"),
      41: (41, 1.0     , "R2" , "Bus4"),

      43: (43, 0.00    , "R2" , "Bus4"),
      44: (44, 0.00    , "R2" , "Bus4"),
      51: (51, 1.0     , "R3" , "Bus5"),

      53: (53, -0.9    , "R3" , "Bus5"),
      54: (54, 0.0     , "R3" , "Bus5"),
      61: (61, 1.0     , "R3" , "Bus6"),

      63: (63, 0.00    , "R3" , "Bus6"),
      64: (64, 0.00    , "R3" , "Bus6"),
      71: (71, 1.0     , "R4" , "Bus7"),

      73: (73, -1.0    , "R4" , "Bus7"),
      74: (74, 0.0     , "R4" , "Bus7"),
      81: (81, 1.00    , "R4" , "Bus8"),

      83: (83, 0.00    , "R4" , "Bus8"),
      84: (84, 0.00    , "R4" , "Bus8"),
      91: (91, 1.0     , "R4" , "Bus9"),

      93: (93, -1.25   , "R4" , "Bus9"),
      94: (94, 0.00    , "R4" , "Bus9"),
   }









   def __init__(self, myHostName):
      self.hostName = myHostName

   def whoIsResponsableFor(self, index):
      if index not in self.data:
         return INVALID_INDEX
      return self.data[index][2]

   def valueFor(self, index):
      if index not in self.data:
         return False, -1
      if self.data[index][2] != self.hostName: # check if owner of information
         return False, -1
      return True, self.data[index][1]


if __name__ == "__main__":
   pass



