#!/usr/bin/env python2

import socket
import traceback
from N_DNS import *
import time

## will manage connections
# createBindInSocket() it will create and bind itself to a socket and listen
#  wait for a new connection when waitForConnection() is called

# connect() will connect to a host 






class CM:
   # dns
   # canIn
   # canOut
   # inSocket
   # outSockets 

   def __init__(self,  myHostname, maxConnections = 5):
      self.dns = N_DNS()
      self.outSockets = {}
      self.MAX_NUMB_CONN = maxConnections

      
   ## server
   def createBindInSocketToHostname(self, hostName):
      myRecord = self.dns.whoIs(hostName)
      return self.createBindInSocketToIP(myRecord[1], myRecord[2])
      
   def createBindInSocketToIP(self, IP, port):
      inSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      inSocket.bind((IP, port))
      #serversocket.bind((socket.gethostname(), 80))
      inSocket.listen(self.MAX_NUMB_CONN)
      return inSocket

   def waitForConnection(self, sockt):
       return  sockt.accept() # return conn, addr


  ## client
   def connectToHostname(self, destHostname):
      record = self.dns.whoIs(destHostname)
      return self.connectToIP(record[1], record[2])
      #self.connect(IP, port)

   def connectToIP(self, IP, port): # maybe rename to connect to
      sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      keepAliveStatus = sock.setsockopt( socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)


      # if( keepAliveStatus == 0):
      #    print 'Socket Keepalive off, turning on'
      #    keepAliveStatus = sock.setsockopt( socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
      #    print 'setsockopt=', keepAliveStatus
      # else:
      #    print 'Socket Keepalive already on'

      try:
         sock.connect((IP, port))
         return  sock
      except socket.error:
         print 'Socket connect failed! Loop up and try socket again'
         traceback.print_exc()
         time.sleep( 5.0)
         #continue
      
   # BOTH
   def send(self, sockORSocket, data):
      try:
         sockORSocket.send(data)
         return True
      except:
         return False
      

   def receive(self, connORSocket , amount = 1024): 
      # while True:
      data = connORSocket.recv(amount)
         # if not data:
         #    break
         # print(toPrettyHex(data))
      #conn.send(data)
      return data
   def receiveAll(self, connORSocket ): 
      newData = str('')
      data = str('')
      while  not newData:
         # print("getting more data")
         newData = connORSocket.recv(1024)
         data = data+newData
         # if not data:
         #    break
         #print(toPrettyHex(data))
      #conn.send(data)
      return data
         
   def close(self, connORSocket): #disconnect 
      connORSocket.close() # maybe needs try catch






######################################################
############### Testing Functions ####################
######################################################
def serverMain():
   print("Running Server MAIN")
   serverName = "DA"
   serverCM = CM(serverName) # data agregator
   server_socket =  serverCM.createBindInSocketToHostname(serverName)
   while True:
      conn, addr = server_socket.accept()
      print("connection received")
      data = serverCM.receiveAll(conn)
      print("received:", data)
      sendStatus = serverCM.send(conn, data)
      if not sendStatus:
         print("error sending")
      serverCM.close(conn)

def clientMain():
   print("Running client MAIN")
   clientName = "CC"
   serverName = "DA"

   clientCM = CM(clientName) 

   sock = clientCM.connectToHostname(serverName)
   sendStatus = clientCM.send(sock, b'hello world!')
   if not sendStatus:
      print("error sending")
   data = clientCM.receiveAll(sock)
   clientCM.close(sock)
   print("received:", data)

   print('')
   print("lets do it again")
   sock = clientCM.connectToHostname(serverName)
   clientCM.send(sock, b'hello world! 2')
   data = clientCM.receiveAll(sock)
   clientCM.close(sock)
   print("received:", data)

   time.sleep(5)

   print('')
   print("lets do it again")
   sock = clientCM.connectToHostname(serverName)
   clientCM.send(sock, b'hello world! 3')
   data = clientCM.receiveAll(sock)
   clientCM.close(sock)
   print("received:", data)
   print('')

   print("lets do it again")
   sock = clientCM.connectToHostname(serverName)
   clientCM.send(sock, b'hello world! 4')
   data = clientCM.receiveAll(sock)
   clientCM.close(sock)
   print("received:", data)

def printHelp():
   print("To test module lunch two instances of this module as follows")
   print("server: ./CM.py 0")
   print("client: ./CM.py 1")





if __name__ == "__main__":
   # to test corrently lunch two instances of this module
   # server: ./CM.py 0
   # client: ./CM.py 1
   import sys
   import time

   if( len(sys.argv) != 2):
      printHelp()
   elif(int(sys.argv[1]) == 0):
      serverMain()
   elif(int(sys.argv[1]) == 1):
      clientMain()
   else:
      printHelp()




