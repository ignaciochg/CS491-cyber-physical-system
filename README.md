# CS491 Cyber-Physical System

CS 491/691 Special Topics on Cyber-Physical System
Socket Communication to Simulate Data Acquisition in Cyber-Physical Systems

# INFO
Developed for Python 2.7
## Components

### Scripts
CC.py = Control Center  
DA.py = Data Aggregator  
R.py = Relay
CM.py = connection manager

Shebangs are used so each component can be run by executing `./<script-name>.py`.   
If that does not work, then run with `python <script-name>`.


## Control Center Script Operation
to run:
```bash
./CC.py
```
### Features
   - [ ] Connect to DA based on values(IP, port) of mp1.pdf/Fig2
   - [x] Before sending request CC should:
      - [x] Calculate length of message
      - [x] Populate Length field in message
   - [ ] Send periodic request to DA for all indices in table of mp1.pdf
      - [ ] Period of 5 seconds === f=1/5s = 0.2Hz = 1 every 5 seconds
   - [x] When response from DA is received:
      - [x] Decode and unpack byte stream according to protocol specs (mp1.pdf)
      - [x] Check that Response Length is valid
      - [ ] Print one value per line
      - [ ] Print in format: CurrentTime(Epoch):Index:Value
   - [ ] ? Interactive shell

## Data Aggregator Script Operation
to run:
```bash
./DA.py
```
### Features
   - [x] TCP Server on port specified by mp1.pdf/Fig2
   - [ ] TODO REST OF THIS SECTION

## Relay Script Operation
To run Relay we have to pass in what relay we want to operate 
example for relay 3:

```bash
./R.py 3
```
### Features
   - [x] TODO THIS SECTION

## Other Components
### N_DNS
Is a local fake DNS which will map names to IP and PORT, which makes it easier for development and deployment.

### DNP3m
`indexes` are of type `usigned int`, of size 1Byte
`values`  are of value `float`, of size 4Bytes
# Notes
https://www.one-tab.com/page/EKewGD_WT4Wz-TTFMvmW2w     

https://www.one-tab.com/page/maLeEs2mTzurEjPVcbw00Q 

https://www.one-tab.com/page/tL430YCHTYG0Aq6BVfcHwQ   
https://www.one-tab.com/page/vELJdJQITsim5pArLBJLVg   