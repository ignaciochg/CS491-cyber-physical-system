global Track: table[count] of double;
global CC: addr = 10.0.0.1;
global DA: addr = 10.0.0.20;
global Relays: set[addr] = {10.0.0.11, 10.0.0.12, 10.0.0.13, 10.0.0.14};
global threshold: double = 0.5;

event bro_init()
	{
	print "starting bro ...";
	}

event dnp3m_request(c: connection, is_orig: bool, start: count, len: count, indices: index_vec)
	{
 	print "dnp3m request";
	}

event dnp3m_response(c: connection, is_orig: bool, start: count, len: count)
	{
	print "dnp3m response";
	}

event dnp3m_mt(c: connection, is_orig: bool, index: count, value: count)
	{
   	local tempInt : int;
   	local tempValue: double;
   	tempInt = value;
   	tempValue = tempInt / 100.00;	
   	#print fmt("dnp3m_mt %s -> %s %d %d %d %f", c$id$orig_h, c$id$resp_h, index, value, tempInt, tempValue);
   	#print fmt("dnp3m_mt %s -> %s %d %f", c$id$orig_h, c$id$resp_h, index, tempValue);

      local dst: addr = c$id$orig_h;
      local src: addr = c$id$resp_h;
      if(src in Relays && dst == DA){
         Track[index] = tempValue;
         return;
      }
      if(src == DA && dst == CC){
         local previous: double = Track[index];
         local bot: double  = previous - threshold;
         local top: double = previous + threshold;
         
         
         if(tempValue < bot || tempValue > top ){
            print fmt("MITM!!  %s -> %s %d 1: %f 2: %f", src, dst, index, previous, tempValue);
         }
         if(index in Track){
            delete Track[index];
         }
         return;
      }
	}

