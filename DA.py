#!/usr/bin/env python2

from DNP3m import * 
#from N_DNS import *
from CM import *
from N_Data import *

myHostName = "DA"
myCM = CM(myHostName) # data agregator
server_socket =  myCM.createBindInSocketToHostname(myHostName)
myData = N_Data(myHostName) # will only know who is responsable for what indexes



MITMvalues = {
   13 : (13, 0.67000),
   23 : (23, 1.79000),
   33 : (33, 1.02065),
   43 : (43, 0.22634),
   53 : (53,-1.06752),
   63 : (63,-0.11182),
   73 : (73,-1.13889),
   83 : (83, 0.10311),
   93 : (93,-1.49187)
}

exit = False
while not exit:

   print("Waiting for new connection")
   conn, addr = server_socket.accept()
   print("Connection received")
   rawData = myCM.receiveAll(conn)

   responseList = []
   cleanResponseList = []
   requestList = {
                  "R1": [],
                  "R2": [],
                  "R3": [],
                  "R4": []
                 }

   mType, content = decodeMessage(rawData)
   if(validContent(mType, content) and mType == REQUEST_BYTE):
      for index in content:
         responsable = myData.whoIsResponsableFor(index)
         if responsable == INVALID_INDEX:
            continue
         requestList[responsable].append(index)
      for key in requestList:
         if key == INVALID_INDEX: # we sont want to connect to INVALID_INDEX, these values will get skipped
            continue
         sock = myCM.connectToHostname(key)
         binRequest = buildRequest(requestList[key])
         sendStatus = myCM.send(sock, binRequest)
         if sendStatus:
            rawResponse = myCM.receiveAll(sock)
            myCM.close(sock)
            mTypeResp, contentResp = decodeMessage(rawResponse)
            if(validContent(mTypeResp, contentResp) and mTypeResp == RESPONSE_BYTE):
               for tup in contentResp:
                  if tup[0] in MITMvalues:
                     print("Changing "+ str(tup[0]))
                     responseList.append(MITMvalues[tup[0]])
                  else:
                     responseList.append(tup)
                  cleanResponseList.append(tup)
         else:
            print("error sending")
   
   #print("will reply to request with", responseList)
   printResponseTable(cleanResponseList)
   binResponse = buildResponse(responseList)
   sendStatus = myCM.send(conn, binResponse)
   if not sendStatus:
      print("error sending")
   myCM.close(conn)

   #print("one more")
   #exit = True


















#       import socket
# MAX_NUMB_CONN = 5
# validIndexes = [11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44,51,52,
#                 53,54,61,62,63,64,71,72,73,74,81,82,83,84,91,92,93,94]
# def requestAllIndexes():
#    pass
# HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
# PORT = 65432        # Port to listen on (non-privileged ports are > 1023)
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# s.bind((HOST, PORT))
# #serversocket.bind((socket.gethostname(), 80))
# s.listen(MAX_NUMB_CONN)
# exit = False
# while not exit:
#    conn, addr = s.accept()
#    print('Connected by', addr)
#    while True:
#       data = conn.recv(1024)
#       if not data:
#          break
#       print(toPrettyHex(data))
#       conn.send(data)
#    print "one more"
#    #exit = True

