#!/usr/bin/env python2

from DNP3m import * 
#from N_DNS import *
from CM import *
from N_Data import *
import sys

def printHelp(ext = False, why = ""):
   print("Relay Data Node Help:")
   print("Usage: ")
   print("    ./R.py <1|2|3|4>")
   print("")
   print("<1|2|3|4>       Number of the Relay node")
   print("")
   print("Notes:")
   print("    Each relay node only has acces and will only respond to the their corresponding indexes only.")
   print("")
   if ext:
      sys.exit(why)


if(len(sys.argv) == 2 ):
   rNumb = int(sys.argv[1])
   if(rNumb == 1):
      myHostName = "R1"
   elif(rNumb == 2):
      myHostName = "R2"
   elif(rNumb == 3):
      myHostName = "R3"
   elif(rNumb == 4):
      myHostName = "R4"
   else:
      printHelp(True ,"ERROR: invalid Relay number")
   print("Running as "+ myHostName)
else:
   printHelp(True ,"ERROR: invalid number of arguments")


# sys.exit("flag0")

myCM = CM(myHostName) # data agregator
server_socket =  myCM.createBindInSocketToHostname(myHostName)
myData = N_Data(myHostName) # this makes it so that each hostname can only acess the data they are responsable for

exit = False
while not exit:

   print("Waiting for new connection")
   conn, addr = server_socket.accept()
   print("Connection received")
   rawData = myCM.receiveAll(conn)

   responseList = []
   requestList = {
                  "R1": [],
                  "R2": [],
                  "R3": [],
                  "R4": []
                 }

   mType, content = decodeMessage(rawData)
   if(validContent(mType, content) and mType == REQUEST_BYTE):
      for index in content:
         #get the index value and then push tuple to list
         found, value = myData.valueFor(index)
         if found:
            responseList.append((index, value))


   #print("will reply to request with", responseList)
   printResponseTable(responseList)
   binResponse = buildResponse(responseList)
   sendStatus = myCM.send(conn, binResponse)
   if not sendStatus:
      print("error sending response")
   myCM.close(conn)

   #print("one more")
   #exit = True